<?php
require __DIR__ . '/../vendor/autoload.php';

use Core\Request;
use Core\Routes;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Response;
use Core\ExceptionsHandler;
use Core\Config;
use Core\Exceptions\TelegramRequestException;


if( !Config::get('debug') ) {
    error_reporting(0);
    @ini_set('display_errors', 0);
}

try{
    $matcher = new UrlMatcher(Routes::all(), Request::context() );
    $path = $matcher->match(Request::raw()->getPathInfo());
    $controller = new $path['_controller']();

    $data = $controller->{$path['_method']}();
    if (!is_string($data)){
        $data = json_encode($data);
    }

    $response = new Response(
        $data,
        Response::HTTP_OK,
        array('content-type' => $path['_content-type'] ?: "application/json" )
    );

    $response->send();
}catch ( Throwable $throwable){
    new ExceptionsHandler($throwable);
}
