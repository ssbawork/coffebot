<?php

namespace Core;

use Curl\Curl;

class Sender
{
    private $CURL;

    private static $instance;

    public static function getInstance() : Sender
    {
        if (self::$instance === null) {
            self::$instance = new Sender();
        }
        return self::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct() {
        try{
            $this->CURL = new Curl();
        } catch ( \Exception $exception){
            new ExceptionsHandler( $exception );
        }

    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup() {}

    private function _get($url , $data = []) {
        $this->CURL->get($url , $data);
        return $this->CURL;
    }

    public static function get($url , $data = []){
        return Sender::getInstance()->_get($url , $data);
    }

}
