<?php

namespace Core\Controller;

use Core\Request;
use Handler\Telegram\TelegramHandler;

class TelegramController
{
    public function index(){
        return 'xuy';
    }

    public function setWebhook(){
        return TelegramHandler::setWebhook();
    }

    public function getMe(){
        return TelegramHandler::getMe();
    }

    public function test(){
        return json_encode( Request::raw()->request->all() );
    }

}