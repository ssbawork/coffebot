<?php

namespace Core;

use Core\Services\SentryClient;
use Core\Config as Config;

class ExceptionsHandler
{
    public function __construct(\Throwable $exception) {

        SentryClient::captureException($exception);
        echo "<pre>";
        print_r($exception);
    }
}