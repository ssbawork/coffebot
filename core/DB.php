<?php

namespace Core;

use Core\Config as Config;
use LessQL\Database as ORM;
use Core\Exceptions\DataBaseConnectionException;

class DB
{
    private $PDO;
    private $LessQL;

    private static $instance;

    public static function getInstance() : DB
    {
        if (self::$instance === null) {
            self::$instance = new DB();
        }
        return self::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct() {
        try{
            $dsn = sprintf("%s:host=%s;dbname=%s", Config::get('db.type') , Config::get('db.host') , Config::get('db.database') );
            $this->PDO = new \PDO($dsn , Config::get('db.login') , Config::get('db.pwd') );
            $this->LessQL = new ORM( $this->PDO );
        } catch (\PDOException $exception){
            new ExceptionsHandler( new DataBaseConnectionException( $exception->getMessage() , $exception->getCode() , $exception ) );
        }

    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup() {}

    private function _ORM() {
           return $this->LessQL;
    }

    public static function ORM(){
        return DB::getInstance()->_ORM();
    }

}
