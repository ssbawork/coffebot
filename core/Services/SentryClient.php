<?php

namespace Core\Services;

use Core\Config as Config;
use Raven_Client;

class SentryClient
{
    private $client;

    private static $instance;

    public static function getInstance() : SentryClient
    {
        if (self::$instance === null) {
            self::$instance = new SentryClient();
        }
        return self::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct() {
        $this->client = new Raven_Client( Config::get('sentry.uri') );
        $this->client->install();
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup() {}

    private function _captureException($ex): string {
        return $this->client->captureException($ex);
    }

    public static  function  captureException($ex){
        return SentryClient::getInstance()->_captureException($ex);
    }
}
