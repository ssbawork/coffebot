<?php

namespace Core;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\Routing\RequestContext;

class Request
{
    private $raw;
    private $context;

    private static $instance;

    public static function getInstance() : Request
    {
        if (self::$instance === null) {
            self::$instance = new Request();
        }
        return self::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct() {
        try{
            $this->raw = SymfonyRequest::createFromGlobals();
            $this->context = new RequestContext();
            $this->context->fromRequest($this->raw);
        } catch ( \Exception $exception ){
            new ExceptionsHandler( $exception );
        }
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup() {}

    private function _context() : RequestContext {
        return $this->context;
    }

    public static function context() : RequestContext {
        return Request::getInstance()->_context();
    }

    private function _raw() : SymfonyRequest {
        return $this->raw;
    }

    public static function raw() : SymfonyRequest {
        return Request::getInstance()->_raw();
    }
}

