<?php

namespace Core;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Loader\XmlFileLoader;

class Routes
{
    private $fileLocator;
    private $fileLoader;
    private $routes;

    private static $instance;

    public static function getInstance() : Routes
    {
        if (self::$instance === null) {
            self::$instance = new Routes();
        }
        return self::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct() {
        try{
            $this->fileLocator = new FileLocator(array(__DIR__));
            $this->fileLoader = new XmlFileLoader( $this->fileLocator );
            $this->routes = $this->fileLoader->load('routes.xml');
        } catch ( \Exception $exception ){
            new ExceptionsHandler( $exception );
        }
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup() {}


    private function _all() : RouteCollection {
        return $this->routes;
    }

    public static function all() : RouteCollection {
        return Routes::getInstance()->_all();
    }
}