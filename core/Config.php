<?php

namespace Core;

use Noodlehaus\Config as ConfigProvider;

class Config
{
    private $conf;
    private static $instance;

    public static function getInstance() : Config
    {
        if (self::$instance === null) {
            self::$instance = new Config();
        }
        return self::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct() {
        try{
            $this->conf =  new ConfigProvider( '../config' );
        } catch (\Exception $exception){

        }

    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup() {}

    private function _getData($key, $default = null){
        return $this->conf->get($key, $default);
    }

    private function _allData(){
        return $this->conf->all();
    }

    public static function get($key, $default = null){
        return Config::getInstance()->_getData($key, $default);
    }

    public static function all(){
        return Config::getInstance()->_allData();
    }

}
