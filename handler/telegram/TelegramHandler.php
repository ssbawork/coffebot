<?php

namespace Handler\Telegram;

use Core\Config;
use Core\Sender;
use Core\Routes;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Core\Exceptions\TelegramRequestException;

class TelegramHandler
{
    private static $instance;

    private $tlgGate;

    public static function getInstance() : TelegramHandler
    {
        if (self::$instance === null) {
            self::$instance = new TelegramHandler();
        }
        return self::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct() {
        try{
            $this->tlgGate = sprintf("https://api.telegram.org/bot%s/", Config::get('telegram.token') );
        } catch ( \Exception $exception){
            throw $exception;
        }
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup() {}

    private function _setWebhook() {
        $route = Routes::all()->get("webhookURL");
        if(is_null($route))
            throw  new RouteNotFoundException();
        $webHookURI = sprintf( "https://%s%s/" , $_SERVER['SERVER_NAME'], $route->getPath() );
        $request = Sender::get( $this->tlgGate . "setWebhook" , ["url" => $webHookURI]);

        if ($request->error) {
            //TODO LogCurl
            throw new TelegramRequestException( $request->errorMessage, 500);
        }
        return $request->response ;
    }

    public static function setWebhook(){
        return TelegramHandler::getInstance()->_setWebhook();
    }


    private function _getMe() {
        $request = Sender::get( $this->tlgGate . "getMe" );
        if ($request->error) {
            //TODO LogCurl
            throw new TelegramRequestException( $request->errorMessage, 500);
        }
        return $request->response ;
    }

    public static function getMe(){
        return TelegramHandler::getInstance()->_getMe();
    }


    //getMe :: user

    //setWebhook
    //deleteWebhook
    //getWebhookInfo :: WebhookInfo
    //sendMessage
}

/*
        if ($request->error) {
           //TODO LogCurl
           throw new Exception( (array)$this->CURL->response, 200);

        }
        return json_encode( $this->CURL->response );

*/