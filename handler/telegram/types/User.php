<?php

namespace Handler\Telegram\Types;

class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $is_bot;

    /**
     * @var String
     */
    private $first_name;

    /**
     * @var String
     */
    private $last_name;

    /**
     * @var String
     */
    private $username;

    /**
     * @var String
     */
    private $language_code;

    public function __construct(int $id, bool $is_bot, string $first_name, string $last_name = null, string $username = null, string $language_code = null )
    {
        $this->id = $id;
        $this->is_bot = $is_bot;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->username = $username;
        $this->language_code = $language_code;
    }

}